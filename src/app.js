export class Welcom {
	heading = 'Welcome to Aurelia';
	firstName = 'Brendan';
	lastName = 'Quetineau';

	get fullName() {
		return `${this.firstName} ${this.lastName}`;
	}

	submit(){
		alert(`Welcome, ${this.fullName}!`);
	}
}